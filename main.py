import pandas as pd

from backtest_utilities import PerformanceMatrices


d = './dataset'
df_2800 = pd.read_csv(f'{d}/HSI_data.csv')

# df_2800 = pd.read_csv(f'{d}/2800.HK (1).csv')
df_spy = pd.read_csv(f'{d}/SPY.csv')

df = pd.merge(left=df_2800, right=df_spy, how='left', on=['Date'])
df.columns = ['Date', '2800_open', '2800_high', '2800_low', '2800_close', '2800_adj_close', '2800_volume',
              'spy_open', 'spy_high', 'spy_low', 'spy_close', 'spy_adj_close', 'spy_volume']

df['2800_intraday_change'] = df['2800_close']/df['2800_open'] - 1
df['spy_intraday_change'] = df['spy_close']/df['spy_open'] - 1

backtest_df = df.loc[:, ['Date', '2800_intraday_change', 'spy_intraday_change']]
backtest_df.dropna(inplace=True)
# backtest_df.index = pd.to_datetime(backtest_df['Date'])

backtest_df = backtest_df[backtest_df['Date'] > '2015']
lookback_period = 60

z_socre_threshold = 2

backtest_df['ma'] = backtest_df['2800_intraday_change'].rolling(window=lookback_period).mean()
backtest_df['sd'] = backtest_df['2800_intraday_change'].rolling(window=lookback_period).std()

backtest_df['signal_long'] = (backtest_df['2800_intraday_change'] - backtest_df['ma']) / backtest_df['sd'] > z_socre_threshold
backtest_df['signal_short'] = (backtest_df['2800_intraday_change'] - backtest_df['ma']) / backtest_df['sd'] < -z_socre_threshold

d = pd.Series(0.0, index=backtest_df['Date'])

for i in range(len(backtest_df)):
    if backtest_df['signal_long'].iloc[i]:
        d.iloc[i] = -backtest_df['spy_intraday_change'].iloc[i]
    elif backtest_df['signal_short'].iloc[i]:
        d.iloc[i] = backtest_df['spy_intraday_change'].iloc[i]
    else:
        d.iloc[i] = 0.0

pf = PerformanceMatrices(d)
pf.performance_plot()

